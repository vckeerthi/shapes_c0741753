//
//  activity2.swift
//  ParticleIOSStarter
//
//  Created by chandra keerthi vidyadharani on 2019-11-01.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Particle_SDK


class activity2: UIViewController {
        let USERNAME = "c0741753@mylambton.ca"
         let PASSWORD = "Bhargavi@2510"
         let DEVICE_ID = "44002c000f47363333343437"
         var myPhoton : ParticleDevice?
    
//    var circleRecognizer: CircleRecognizer!

    
    let canvas = Canvas()
    
    let startButton : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("start", for: .normal)
        button.addTarget(self, action: #selector(startPressed(_:)), for: .touchUpInside)
        return button
    }()
    let clearButton : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("clear", for: .normal)
        button.addTarget(self, action: #selector(clearall), for: .touchUpInside)
        return button
    }()
    let checkButton : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("check", for: .normal)
//        button.addTarget(self, action: #selector(startPressed(_:)), for: .touchUpInside)
        return button
    }()
   let answerLabel : UILabel = {
    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
            label.center = CGPoint(x: 0, y: 0)
    label.textAlignment = .center
    label.text = "answer is"
    //        button.addTarget(self, action: #selector(startPressed(_:)), for: .touchUpInside)
            return label
        }()
    
    override func loadView() {
        self.view = canvas
        
//        canvas.accessibilityPath?.isEqual(UIImage(named: "triangle"))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        canvas.backgroundColor = .white
        setuplayout()
        setupParticle()
        getDeviceFromCloud()
//      circleRecognizer = CircleRecognizer(target: self, action: "circled:")
//      view.addGestureRecognizer(circleRecognizer)
//
        
        
    }
    
    func setuplayout(){
        let stackView = UIStackView(arrangedSubviews: [startButton,clearButton,checkButton,answerLabel])
               stackView.frame = CGRect(x: 0, y: 0, width: 200, height: 100)
               view.addSubview(stackView)
               
               stackView.distribution = .fillEqually
               stackView.translatesAutoresizingMaskIntoConstraints = false
               stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
               stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
               stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    func setupParticle(){
        
            ParticleCloud.init()
              
            ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                           
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                            
                print(error?.localizedDescription)
                         }
            else {
                print("Login success!")
                }
        }
    }
    
    
    func getDeviceFromCloud() {
    ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
             print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon: \(device?.id)")
                self.myPhoton = device
            }
            
        }
    }
   
    @objc func handlePan(sender:UIPanGestureRecognizer){
        let fileview = sender.view
        let translation = sender.translation(in: view)

    }
    @objc func startPressed(_ sender: Any) {
        let randomFunc = [self.triangleFunction, self.squareFunction, ]
           let randomResult = Int(arc4random_uniform(UInt32(randomFunc.count)))
           return randomFunc[randomResult]()
    }
    func triangleFunction(){
        let funcArgs = ["triangle",1] as [Any]
        var task = myPhoton!.callFunction("triangle", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("LEDs successfully turned on")
            }
        }
//        var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
    }
    func squareFunction(){
        let funcArgs = ["square",1] as [Any]
               var task = myPhoton!.callFunction("square", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
                   if (error == nil) {
                       print("LEDs successfully turned on")
                   }
               }
    }
    @objc func clearall(){
        canvas.clearall()
        
    }
//    @objc func checkPressed(){
////        if(canvas.accessibilityPath?.isEqual(UIImage(named: "triangle"))==true){
////            print("hi")
////        }
//    }
    var square: UIGestureRecognizer!
    
//   
}
