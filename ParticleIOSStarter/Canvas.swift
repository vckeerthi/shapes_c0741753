//
//  Canvas.swift
//  ParticleIOSStarter
//
//  Created by chandra keerthi vidyadharani on 2019-11-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit

class Canvas:UIView{
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        context.setLineWidth(8)
        context.setLineCap(.butt)
        lines.forEach{(line) in
            for (i,p) in line.enumerated(){
                        if i == 0{
                            context.move(to:p)
//                            print("move")
                        }
                        else{
                            context.addLine(to:p)
            //                print("line")
                        }
                    }
            
        }
        
        context.strokePath()
        
        
    }
//     var line = [CGPoint]()
    var lines = [[CGPoint]]()
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        lines.append([CGPoint]())
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self) else {return}
        guard var lastLine = lines.popLast()  else {
            return
        }
        lastLine.append(point)
        lines.append(lastLine)
        setNeedsDisplay()
    }
    func clearall(){
        lines.removeAll()
        setNeedsDisplay()
    }
}
