#include <InternetButton.h>

InternetButton b = InternetButton();


void setup() {
    b.begin();
    Particle.function("circle", circle);
    Particle.function("square", square);
    Particle.function("triangle", triangle);
    Particle.function("correct", correct);
     Particle.function("incorrect", incorrect);
}


void loop() {
    if(b.buttonOn(1)){
        b.allLedsOff();
    }
     if(b.buttonOn(3)){
       Particle.publish("button3","triangle",60,PRIVATE);
    }
    if(b.buttonOn(4)){
       Particle.publish("button4","square",60,PRIVATE);
    }

}
int circle(String cmd){

    b.allLedsOn(0,0,255);

     return 1;
    }
int square(String cmd){
    b.allLedsOff();
    b.ledOn(4,0,0,255);
    b.ledOn(2,0,0,255);
    b.ledOn(8,0,0,255);
    b.ledOn(10,0,0,255);
    return 1;
}
int triangle(String cmd){
    b.allLedsOff();
    b.ledOn(2,0,0,255);
    b.ledOn(10,0,0,255);
    b.ledOn(6,0,0,255);
    return 1;
}
int correct(String cmd){
    b.allLedsOn(0,255,0);
    return 1;
}
int incorrect(String cmd){
    b.allLedsOn(255,0,0);
    return 1;
}
